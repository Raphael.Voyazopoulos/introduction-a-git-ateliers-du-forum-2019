### [The Pro Git online book](https://git-scm.com/book/en/v2)
Kind of "official" Git tutorial, with a Getting started guide. Explains what is version control, why it is useful, how Git handles version control, how to create your first commit, first branch, etc.

### [GitHub learning resources](https://try.github.io)
GitHub is one of the most iconic Git repositories hosting solution but there's also GitLab.com, and BitBucket to only name a few.
Their learning pages are also a good start to grasp the advantages of using git. That's also where I got the cheat sheets I gave you!

### [Graphical interfaces for Git](https://git-scm.com/downloads/guis)
If the terminal scares you, try one of the many graphical interfaces for all platforms. Personally, I'm using [Sublime Merge](https://www.sublimemerge.com/) because it is light and simple.